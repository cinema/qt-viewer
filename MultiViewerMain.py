import os

from PySide.QtGui import QColorDialog, QFileDialog, QFrame
from PySide.QtGui import QMainWindow, QMessageBox
from PySide import QtCore

from widgets.uiFiles import ui_MultiViewerMain as ui
from widgets import CameraWidget
from widgets import ParameterLinks
from widgets import ControllersWidget
from common.CinemaSpec import CinemaSpec
from common import DatabaseLoader
from rendering.PhiThetaInteractor import PhiThetaInteractor
from rendering.ArcBallInteractor import ArcBallInteractor


class ViewerMain(QMainWindow):
    ''' Uses ControllerWidget and CameraWidget abstractions to support multiple
    side-by-side views.

    Receives a list of Cinema stores and creates a
    ControllerWidget-CameraWidget pair per store instance.  Parameters and
    color controls are fully independent per store (except background color
    which is controlled globally. '''
    def __init__(self, parent=None):
        super(ViewerMain, self).__init__(parent)
        self.__initializeViewer()

    def __initializeViewer(self):
        self.__ui = ui.Ui_ViewerMain()
        self.__cameraWidgets = []
        self.__currentController = None
        self.__currentCamera = None
        self.__colorPicker = QColorDialog(self)
        self.__fileDialog = QFileDialog(self, "Open a Cinema Database", "~",
                                        "Cinema Databases (*.cdb)")

        # Setup ui
        self.__ui.setupUi(self)
        self.__ui.actionExit.triggered.connect(self.close)
        self.__ui.actionOpen.triggered.connect(self.__onOpenFileTriggered)
        self.__ui.pbBgColor.clicked.connect(self.__onBgColorClicked)

    @QtCore.Slot()
    def __onOpenFileTriggered(self):
        ''' Re-initializes the viewer. '''
        if self.__fileDialog.exec_() == self.__fileDialog.Accepted:
            filePath = self.__fileDialog.selectedFiles()[0]

            # look at the file and figure out what it is
            filePath.rstrip('/')

            valid_database = False
            # if it's a cdb directory
            if (filePath.endswith('.cdb')):
                # look for a json file
                if os.path.isfile(os.path.join(filePath, "info.json")):
                    # this is a multi-database
                    filePath = os.path.join(filePath, "info.json")
                    valid_database = True

                elif os.path.isfile(
                        os.path.join(filePath, "image", "info.json")):
                    # this is a regular database
                    filePath = os.path.join(filePath, "image", "info.json")
                    valid_database = True

                # if it's not a json file, bail
                elif (filePath.endswith('.json')):
                    valid_database = True

            if (valid_database):
                databases, links = DatabaseLoader.loadAll(filePath)
                self.setStores(databases, links)
            else:
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("Can't find info.json file")
                msg.setWindowTitle("Warning")
                msg.setStandardButtons(QMessageBox.Ok)
                return msg.exec_()

    @QtCore.Slot()
    def __onBgColorClicked(self):
        if self.__colorPicker.exec_() == self.__colorPicker.Accepted:
            color = self.__colorPicker.selectedColor()

            for cw in self.__cameraWidgets:
                cw.setBackgroundColor(color)
                index = cw.getId()
                controller = self.__ui.stackedControllers.widget(index)
                controller.triggerDisplayUpdate()

    @QtCore.Slot(int)
    def __onCameraSelected(self, cameraId):
        ''' Handles selection (mouse press) of different CameraWidget. Switches
        between ControllerWidgets, etc. '''
        # self.sender() does not work properly so the previously set Id is used
        # to identify the calling camera view.
        if self.__ui.stackedControllers.currentIndex() is not cameraId:

            camera = self.__cameraWidgets[cameraId]
            if (self.__currentCamera):
                self.__currentCamera.setFrameShape(QFrame.NoFrame)
            camera.setFrameShape(QFrame.Panel)
            self.__currentCamera = camera

            self.__ui.stackedControllers.setCurrentIndex(cameraId)
            controller = self.__ui.stackedControllers.currentWidget()
            self.__currentController = controller

    def __createCameraAndControllers(self, store):
        spec = CinemaSpec().resolveStoreVersion(store)

        # Create camera widget
        cw = CameraWidget.CameraWidget(self)
        cw.setSpecification(spec)

        camera_model = store.get_camera_model()
        if camera_model == "phi-theta":
            cw.setInteractor(PhiThetaInteractor())
        if camera_model == "azimuth-elevation-roll":
            cw.setInteractor(ArcBallInteractor())
        if camera_model == "yaw-pitch-roll":
            cw.setInteractor(ArcBallInteractor())

        self.__ui.layoutCameras.addWidget(cw)
        self.__cameraWidgets.append(cw)

        # Create controller widget. Use the ControllersWidget index in the
        # QStackedWidget as a CameraWidgetId.
        control = ControllersWidget.ControllersWidget(self)
        cameraId = self.__ui.stackedControllers.addWidget(control)
        control.setStore(store, spec, cw, cameraId)

        # Connect camera widget to the global switch
        cw.cameraSelected.connect(self.__onCameraSelected)

    def setStores(self, databases, links):
        for cs in databases:
            self.__createCameraAndControllers(cs)

        controllers = []

        # setup parameter links between otherwise completely independent views
        self.__parameterLinks = ParameterLinks.ParameterLinks(links)
        for x in self.__ui.stackedControllers.children():
            if type(x).__name__ == "ControllersWidget":
                controllers.append(x)
                x.SetLinks(self.__parameterLinks)
        self.__parameterLinks.SetControllers(controllers)

        # Force selection update to render the frame
        if len(databases) > 1:
            self.__ui.stackedControllers.setCurrentIndex(1)
            self.__onCameraSelected(0)
